# What does it do?
TL;DR when you (left-, middle- or right-) click a `<h3>` search result, rewrite
the `a`'s `href` to be the target page, not the Google tracking page.

When you click a search result on Google search, some javascript rewrites the `href`
attribute of the `a` so you get sent via the Google tracking page. Makes total sense why, but
I wondered how much that extra hop is slowing down my navigation to the page I *actually*
want to go to.

By changing the URL, you save navigation time because you go direct to the
target URL and not to the Google tracking page, then get redirected to the
target URL. You can also right click and "copy link" direct from the search
results and get the real URL, not the Google tracking one.

# Why?
To see if nav is faster without going via Google tracking. I'm not convinced it is because
the fact I can see my browser on the tracking page for a (human) noticeable amount of time
is probably because the final destination site is still sending bytes down the wire.

It was a fun exercise anyway.

# How do I use it?
It's written as a Greasemonkey/Tampermonkey script. But you can also test it on a single
page by
- copying all the code from index.html
- removing references to `unsafeWindow.`
- paste and run the code in the browser devtools console
- click a search result heading (the `<h3>`)

# Is it robust and well-tested?
Nope. I hacked it together and only tested it on one browser with a single search page.
Use at your own peril.

# License
Do whatever you want it with 🤷.

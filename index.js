// ==UserScript==
// @name         faster google search clicks
// @version      0.1
// @description  Unmangle the URL to go direct to the site, not via google tracking
// @match        https://www.google.com/search*
// @grant        unsafeWindow
// ==/UserScript==

function linkUnmangler(e) {
  const srcE = e.srcElement
  if (srcE.localName !== 'h3' || srcE.parentElement.localName !== 'a') {
    unsafeWindow.console.log('clicked something *not* of interest')
    return true
  }
  unsafeWindow.console.log('clicked result heading of interest')
  const mangledUrl = srcE.parentElement.href
  const isMangled = mangledUrl.startsWith('https://www.google.com/url')
  if (!isMangled) {
    return true
  }
  const actualTargetUrl = decodeURIComponent(mangledUrl.split('&').find(e => e.startsWith('url=')).replace(/^url=/, ''))
  unsafeWindow.console.log('going to', actualTargetUrl)
  srcE.parentElement.href = actualTargetUrl
}

unsafeWindow.console.log('Adding click handlers')
const searchResultsContainer = document.querySelector('div#center_col')
searchResultsContainer.addEventListener('click', linkUnmangler)
searchResultsContainer.addEventListener('auxclick', linkUnmangler)
searchResultsContainer.addEventListener('contextmenu', linkUnmangler)
unsafeWindow.console.log('Click handlers added')
